.. Микрон Инвентаризация documentation master file, created by
   sphinx-quickstart on Mon Jun  8 13:16:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Микрон Инвентаризация
===============================================================================

Добро пожаловать в документацию клиентского приложения "Микрон Инвентаризация"!

.. toctree::
   :maxdepth: 2

   login
   object-list
   object-create
   object-details
